package com.example.sharedperferences

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.sharedperferences.databinding.ActivityMainBinding
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var sharedPreferences : SharedPreferences
    private lateinit var editor : SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupSharedPreferences()
        checkLogin()
        login()

    }

    private fun setupSharedPreferences() {
        sharedPreferences = getSharedPreferences("login",Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    private fun checkLogin() {
//        var username = sharedPreferences.getString("username","")
//        var password = sharedPreferences.getString("pw","")
//        if(username != "" && password != ""){
//            moveToHelloActi()
//        }
        var gson = Gson()
        var json = sharedPreferences.getString("user","")
        var user : User = gson.fromJson(json,User::class.java)
        if(user.username != "" && user.pw != "")
        {
            moveToHelloActi()
        }
    }

    private fun login() {
        binding.btn.setOnClickListener(View.OnClickListener {
            var username = binding.tvUsername.text.toString()
            var password = binding.tvPassword.text.toString()
            if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
                Toast.makeText(this, "empty", Toast.LENGTH_SHORT).show()
            }
            else{
                var user : User = User(username,password)
                var gson = Gson()
                var json: String = gson.toJson(user)
//                editor.putString("username",username)
//                editor.putString("pw",password)
                editor.putString("user",json)
                editor.commit()
                moveToHelloActi()
            }
        })
    }

    private fun moveToHelloActi(){
        var intent = Intent(this@MainActivity,HelloActivity::class.java)
        startActivity(intent)
    }
}