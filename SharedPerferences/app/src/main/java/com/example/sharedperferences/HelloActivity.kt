package com.example.sharedperferences

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.sharedperferences.databinding.ActivityHelloBinding
import com.google.gson.Gson

class HelloActivity : AppCompatActivity() {
    private lateinit var binding : ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
        var gson = Gson()
        var json = sharedPreferences.getString("user","")
        var user : User = gson.fromJson(json,User::class.java)
        binding.tv.text = user.username

        binding.btn.setOnClickListener(View.OnClickListener {
            var editor = sharedPreferences.edit()
            editor.clear()
            editor.commit()
            finish()
        })

    }
}